module MyTests where

import Test.Tasty (TestTree(..), testGroup)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib

myTests :: [TestTree]
myTests = [ 
			testGroup "Sem1" [
      testGroup "MyTests"
            [ 
		testCase "test1" $ eval1' (Apply(Apply(Lambda "x" (Lambda "y" (Apply(Param "x") (Param "y")))) (Lambda "z" (Param "z"))) (Lambda "u" (Lambda "v" (Param "v")))) @?= Apply (Lambda "y0" (Apply (Lambda "z" (Param "z")) (Param "y0"))) (Lambda "u" (Lambda "v" (Param "v"))),
		testCase "test2" $ full (Apply(Apply(Lambda "x" (Lambda "y" (Apply(Param "x") (Param "y")))) (Lambda "z" (Param "z"))) (Lambda "u" (Lambda "v" (Param "v")))) @?= Lambda "u" (Lambda "v" (Param "v")),
		testCase "test3" $ eval1' (Apply (Lambda "x" $ Lambda "y" $ Param "x") (Lambda "y" $ Param "y")) @?= Lambda "y0" (Lambda "y" (Param "y"))  
		]]
          ]

import Test.Tasty (TestTree(..), defaultMainWithIngredients, testGroup)
import Test.Tasty.Ingredients.Basic (consoleTestReporter, listingTests)
import Test.Tasty.Runners.AntXML (antXMLRunner)

import MyTests (myTests)

main :: IO ()
main = defaultMainWithIngredients [antXMLRunner, consoleTestReporter, listingTests] tests

tests :: TestTree
tests = testGroup "Tests"
        [ testGroup "MyTests" myTests
       	  
        ]

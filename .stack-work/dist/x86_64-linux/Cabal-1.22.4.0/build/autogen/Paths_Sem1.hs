module Paths_Sem1 (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/dmitriy/Projects/FP/Sem/Sem1/.stack-work/install/x86_64-linux/lts-3.20/7.10.2/bin"
libdir     = "/home/dmitriy/Projects/FP/Sem/Sem1/.stack-work/install/x86_64-linux/lts-3.20/7.10.2/lib/x86_64-linux-ghc-7.10.2/Sem1-0.1.0.0-6L5tIeorTlyHfnVWhpLqIf"
datadir    = "/home/dmitriy/Projects/FP/Sem/Sem1/.stack-work/install/x86_64-linux/lts-3.20/7.10.2/share/x86_64-linux-ghc-7.10.2/Sem1-0.1.0.0"
libexecdir = "/home/dmitriy/Projects/FP/Sem/Sem1/.stack-work/install/x86_64-linux/lts-3.20/7.10.2/libexec"
sysconfdir = "/home/dmitriy/Projects/FP/Sem/Sem1/.stack-work/install/x86_64-linux/lts-3.20/7.10.2/etc"

getBinDir, getLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "Sem1_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "Sem1_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "Sem1_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "Sem1_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "Sem1_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)

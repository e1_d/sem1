module Sem1 where

import Data.List

data Term = Lambda String Term 
          | Param String   
          | Apply Term Term 
          deriving (Show,Eq)

isFree :: Term -> String -> Bool
isFree (Param p) s = (p == s)

isFree (Lambda l t) s | l == s = False
		      | otherwise = isFree t s

isFree (Apply t1 t2) s = (isFree t1 s) && (isFree t2 s)
	

isBound :: Term -> String -> Bool
isBound (Param p) s = False
isBound (Lambda l t) s = (l == s) || (isBound t s)
isBound (Apply t1 t2) s = (isBound t1 s) || (isBound t2 s)

rename :: Term -> String -> String -> Term
rename t s1 s2 =
  if (isFree t s2) || (isBound t s2) then undefined else
    case t of
      Param p -> if p == s1 then (Param s2) else Param p
      (Lambda l tm) -> if l == s1 then (Lambda s2 (rename tm s1 s2)) else (Lambda l (rename tm s1 s2))
      (Apply t1 t2) -> (Apply (rename t1 s1 s2) (rename t2 s1 s2))

newvar :: String -> Term -> Integer -> String
newvar i t n =
  let i' = (i ++ (show n)) in
  if (isFree t i') || (isBound t i') then newvar i t (n + 1) else i'

bReduction :: String -> Term -> Term -> Term
bReduction toReplace replacingWith na@(Param name)  = if toReplace == name then replacingWith else na
bReduction toReplace replacingWith (Apply term1 term2) = (Apply (bReduction toReplace replacingWith term1) (bReduction toReplace replacingWith term2))
bReduction toReplace replacingWith (Lambda name term)  = let a = (newvar name replacingWith 0) 
                                                        in (Lambda a (bReduction toReplace replacingWith (rename term name a)))

canBeEvaluated :: Term -> Bool
canBeEvaluated term = case term of
	Param p -> False
	Apply (Param x) _ -> False
	Lambda x y -> canBeEvaluated y
	_ -> True 


eval :: Term -> Term
eval term | canBeEvaluated term = eval1' term
	  | otherwise = term


eval1' (Apply (Lambda l1 t1) t2) = (bReduction l1 t2 t1)
eval1' (Apply t1 t2) = (Apply (eval1' t1) t2)
eval1' (Lambda l t) = (Lambda l (eval t))

full :: Term -> Term
full term | canBeEvaluated term = full (eval term)
          | otherwise = term
